#! /bin/bash

### input : nom du dossier / sass ? / js ? / git ? ###

read -p "nom du dossier : " foldername
read -p "utiliser sass \"y\" ou \"n\" ? " sass
read -p "utiliser javascript \"y\" ou \"n\" ? " js
	
### creer architecture dossier ###

mkdir $foldername
cd $foldername
mkdir src
touch index.html
touch README.md

# init avec sass et javascript
if [ $sass = "y" ] && [ $js = "y" ]
then
	# creation dossier sass + fichier.scss
	mkdir src/sass
	touch src/sass/index.scss
	echo "* {
	padding: 0px;
	margin: 0px;
	color: #0CEE6C;
}" >> src/sass/index.scss
	# creation dossier js + fichier.js
	mkdir src/js
	touch src/js/index.js
	echo "window.alert(\"javascript en place\");" >> src/js/index.js
	# edition fichier.html
	echo "<!DOCTYPE html>
<html lang=\"fr\">
	<head>
		<meta charset=\"UTF-8\"/>
		<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>
		<meta name=\"robots\" content=\"index\"/>
		<!-- META -->
		<meta name=\"description\" content=\"\">
		<title></title>
		<!-- CUSTOM CSS -->
		<link rel=\"stylesheet\" href=\"public/index.css\"/>
		<!-- FAVICON -->
		<link rel=\"icon\" href=""/>
	</head>
​	<body>
		<h1>Hello world</h1>
​		<!-- CUSTOM JS -->
		<script type=\"text/javascript\" src=\"src/js/index.js\"></script>
​	</body>
</html>" >> index.html

# init sans sass et avec javascript
elif [ $sass = "n" ] && [ $js = "y" ]
then
	# creation dossier css + fichier.css
	mkdir src/css
	touch src/css/index.css
	echo "* {
	padding: 0px;
	margin: 0px;
	color: #0CEE6C;
}" >> src/css/index.css
	# creation dossier js + fichier.js
	mkdir src/js
	touch src/js/index.js
	echo "window.alert(\"javascript en place\");" >> src/js/index.js
	# edition fichier.html
	echo "<!DOCTYPE html>
<html lang=\"fr\">
	<head>
		<meta charset=\"UTF-8\"/>
		<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>
		<meta name=\"robots\" content=\"index\"/>
		<meta name=\"description\" content=\"\">
		<title></title>
		<!-- CUSTOM CSS -->
		<link rel=\"stylesheet\" href=\"src/css/index.css\"/>
		<!-- FAVICON -->
		<link rel=\"icon\" href=""/>
	</head>
​	<body>
		<h1>Hello world</h1>
​		<!-- CUSTOM JS -->
		<script type=\"text/javascript\" src=\"src/js/index.js\"></script>
​	</body>
</html>" >> index.html

# init avec sass et sans javascript
elif [ $sass = "y" ] && [ $js = "n" ]
then
	# creation dossier css + fichier.css
	mkdir src/sass
	touch src/sass/index.scss
	echo "* {
	padding: 0px;
	margin: 0px;
	color: #0CEE6C;
}" >> src/sass/index.scss
	# edition fichier.html
	echo "<!DOCTYPE html>
<html lang=\"fr\">
	<head>
		<meta charset=\"UTF-8\"/>
		<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>
		<meta name=\"robots\" content=\"index\"/>
		<!-- META -->
		<meta name=\"description\" content=\"\">
		<title></title>
		<!-- CUSTOM CSS -->
		<link rel=\"stylesheet\" href=\"public/index.css\"/>
		<!-- FAVICON -->
		<link rel=\"icon\" href=""/>
	</head>
​	<body>
		<h1>Hello world</h1>
​		<!-- CUSTOM JS -->
​	</body>
</html>" >> index.html

# init sans sass et sans javascript
elif [ $sass = "n" ] && [ $js = "n" ]
then
	# creation dossier css + fichier.css
	mkdir src/css
	touch src/css/index.css
	echo "* {
	padding: 0px;
	margin: 0px;
	color: #0CEE6C;
}" >> src/css/index.css
	# edition fichier.html
	echo "<!DOCTYPE html>
<html lang=\"fr\">
	<head>
		<meta charset=\"UTF-8\"/>
		<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>
		<meta name=\"robots\" content=\"index\"/>
		<!-- META -->
		<meta name=\"description\" content=\"\">
		<title></title>
		<!-- CUSTOM CSS -->
		<link rel=\"stylesheet\" href=\"src/css/index.css\"/>
		<!-- FAVICON -->
		<link rel=\"icon\" href=""/>
	</head>
​	<body>
		<h1>Hello world</h1>
​		<!-- CUSTOM JS -->
​	</body>
</html>" >> index.html
fi

### NPM ###

# si on utilise javascript
if [ $js = "y" ]
then
	# demande si l'on utilise npm
	read -p "utiliser npm \"y\" ou \"n\" ? " npm

	if [ $npm = "y" ]
	then
		npm install
		npm init -y
		# demande si l'on utilise webpack
		read -p "utiliser webpack \"y\" ou \"n\" ? " webpack

		if [ $webpack = "y" ]
		then
			# installe webpack et creer le fichier de config webpack
			npm install webpack webpack-cli --save-dev
			touch webpack.config.js
			echo "const path = require('path');

module.exports = {
	mode: 'development',
	entry: {
		index: \"./src/js/index.js\"
	},
	output: {
		filename: '[name].js',
		path: path.resolve(__dirname, 'public')
	},
};" >> webpack.config.js

			# si le projet utilise sass
			if [ $sass = "y" ]
			then
					npm install sass-loader sass style-loader css-loader mini-css-extract-plugin --save-dev
					echo "const path = require('path');
	const MiniCssExtractPlugin = require('mini-css-extract-plugin');

	module.exports = {
		mode: 'development',
		// mode: 'production',
		entry: {
			index: \"./src/js/index.js\"
		},
		output: {
			filename: '[name].js',
			path: path.resolve(__dirname, 'public')
		},
	  	plugins: [
		  	new MiniCssExtractPlugin({
		  		filename: '[name].css'
		  	})
	  	],
		module: {
			rules: [
				{
					test: /\.css$/,
					use: [
						'style-loader',
						'css-loader'
					]
				},
				{
					test: /\.scss$/,
					use: [
						MiniCssExtractPlugin.loader,
						'css-loader',
						'sass-loader',
					]
				}
			]
		}
	};" > webpack.config.js
					echo "import \"../sass/index.scss\";

	window.alert(\"javascript en place\");" > src/js/index.js
					echo "<!DOCTYPE html>
	<html lang=\"fr\">
		<head>
			<meta charset=\"UTF-8\"/>
			<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>
			<meta name=\"robots\" content=\"index\"/>
			<!-- META -->
			<meta name=\"description\" content=\"\">
			<title></title>
			<!-- CUSTOM CSS -->
			<link rel=\"stylesheet\" href=\"public/index.css\"/>
			<!-- FAVICON -->
			<link rel=\"icon\" href=""/>
		</head>
	​	<body>
			<h1>Hello world</h1>
	​		<!-- CUSTOM JS -->
			<script type=\"text/javascript\" src=\"public/index.js\"></script>
	​	</body>
	</html>" > index.html
			else
				echo "<!DOCTYPE html>
	<html lang=\"fr\">
		<head>
			<meta charset=\"UTF-8\"/>
			<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>
			<meta name=\"robots\" content=\"index\"/>
			<!-- META -->
			<meta name=\"description\" content=\"\">
			<title></title>
			<!-- CUSTOM CSS -->
			<link rel=\"stylesheet\" href=\"src/css/index.css\"/>
			<!-- FAVICON -->
			<link rel=\"icon\" href=""/>
		</head>
	​	<body>
			<h1>Hello world</h1>
	​		<!-- CUSTOM JS -->
			<script type=\"text/javascript\" src=\"public/index.js\"></script>
	​	</body>
	</html>" > index.html
			fi
		fi
	fi
fi

### git ###

read -p "utiliser git \"y\" ou \"n\" ? " git

# init avec git
if [ $git = "y" ]
then
	git init
	touch .gitignore
	echo ".gitignore
.DS_Store
src/.DS_Store" >> .gitignore
	
	if [ $git = "y" ] && [ $webpack = "y" ]
	then
		echo "node_modules" >> .gitignore
	fi
fi

### messages informatifs ###

echo -e "\n********************"
echo -e "INFORMATIONS\n "

if [ $sass = "y" ]
then
		
	if [ $webpack = "y" ]
	then
		echo "executer \" npx webpack --watch \" pour compiller Javascript et Sass"
	else
		echo "executer \" sass --watch src/sass/index.scss public/index.css \" pour compiller Sass"
	fi
fi

if [ $sass = "n" ] && [ $webpack = "y" ]
then
	echo "\" npx webpack --watch \" pour compiller Javascript"
fi

if [ $git = "y" ]
then
	echo -e "projet initialiser avec git\n\" git remote add origin *SSH du repository* \" pour lier a un repository"
fi

echo -e "\n********************"
echo -e "SCRIPT TERMINE\n "